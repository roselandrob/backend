import os
import requests
import json
import logging
import http.client as http_client

from .utils import get_access_token


def mfa_push_handler(event, context):
    http_client.HTTPConnection.debuglevel=1
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
    access_token = get_access_token()
    body_dict = json.loads(event["body"])
    print(body_dict)
    hypr_user_name = event["requestContext"]["authorizer"]["claims"]["sub"]
    name_for_message = body_dict["name"]
    city = body_dict["city"]
    hypr_url = os.environ.get('HYPR_URL')
    hypr_application = os.environ.get('HYPR_APPLICATION')
    print("Requesting devices")
    response = requests.get("https://" + hypr_url + "/rp/api/oob/client/authentication/" + hypr_application + "/"
                            + hypr_user_name + "/devices",
                            headers={'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access_token})
    print("Response is: " + response.text)
    machine_id = response.json()[0].get("machineId")
    print("Machine ID is: " + machine_id)
    transaction_request_body = {
        "machineId": machine_id,
        "namedUser": hypr_user_name,
        "machine": "Personal Account",
        "deviceNonce": "3859325d5afae6bfe8140cf5198a45be0d24c8d6aa168e51a1ea6c0ab2535a0e",
        "sessionNonce": "1430a4bcd6c952d53d9a5b248d43f4101bbde5e6a703486a0738df561b600227",
        "serviceNonce": "96b486de52d8e173366d4c6a0048441cabff491c08f2c055573d2c780e118b77",
        "serviceHmac": "nonimportanthmac",
        "appId": hypr_application,
        "actionId": "completeMediumTransaction",
        "transactionText": "Request for " + name_for_message + " in city: " + city + ".",
        "transactionType": "Postman"
    }
    print(transaction_request_body)
    push_response = requests.post(
        'https://' + hypr_url + '/rp/api/oob/client/authentication/requests/transaction',
        headers={'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access_token},
        json=transaction_request_body)
    print("Push response: " + push_response.text)
    return {
        "statusCode": 200,
        "body": json.dumps({
            "requestId": push_response.json().get("response").get("requestId")
        }),
        "headers": {
            "Content-Type": "application/json",
            "x-custom-header": "my custom header value",
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        }
    }
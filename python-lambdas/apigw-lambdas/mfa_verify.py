import os
import requests
import json
import logging
import http.client as http_client

from .utils import get_access_token


def mfa_verify_handler(event, context):
    http_client.HTTPConnection.debuglevel = 1
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
    access_token = get_access_token()
    body_dict = json.loads(event["body"])
    print(body_dict)
    request_id = body_dict["requestId"]
    name_for_message = body_dict["name"]
    city = body_dict["city"]
    hypr_url = os.environ.get('HYPR_URL')
    print("Requesting devices")
    response = requests.get("https://" + hypr_url + "/rp/api/oob/client/authentication/requests/" + request_id,
                            headers={'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access_token})
    print("Response is: " + response.text)
    is_successful = any([state["value"] == "COMPLETED" for state in response.json()["state"]])
    print(f"Is successful: {is_successful}")
    if is_successful:
        return {
            "statusCode": 200,
            "body": json.dumps({
                "message": "Welcome, " + name_for_message + " from " + city + "."
            }),
            "headers": {
                "Content-Type": "application/json",
                "x-custom-header": "my custom header value",
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            }
        }
    else:
        return {
            "statusCode": 401,
            "headers": {
                "Content-Type": "application/json",
                "x-custom-header": "my custom header value",
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            }
        }

terraform {
  backend "local" {
    path = "/Users/robcossin/iCloud/tfstate/helloworld_service/terraform.tfstate"
  }
}

# locals {
#   cpu = "256"
#   memory = "512"
#   service_config = templatefile("service.json", { cpu = local.cpu, memory = local.memory, port = "3000" })
# }

provider "aws" {
  region = "us-east-2"
}

resource "aws_vpc" "svc_vpc" {
   cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "svc_subnet" {
  vpc_id     = aws_vpc.svc_vpc.id
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "Main"
  }
}

resource "aws_secretsmanager_secret" "hypr_key" {
  name_prefix="hypr_key"
}

# resource "aws_ecr_repository" "web" {
  # name                 = "web_repo"
  # image_tag_mutability = "MUTABLE"
# }

# resource "aws_ecs_cluster" "cluster" {
  # name = "web_cluster"
# }


# resource "aws_lb" "test" {
#   name                             = "test-lb-tf"
#   internal                         = false
#   load_balancer_type               = "network"
#   enable_cross_zone_load_balancing = true
#   subnets                          = data.aws_subnet_ids.example.ids

#   tags = {
#     Environment = "test"
#   }
# }

# resource "aws_lb_listener" "listener" {
#   load_balancer_arn = aws_lb.test.arn
#   port              = 3000
#   protocol          = "TCP"
#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.target.arn
#   }
# }

# locals {
#   cpu = "256"
#   memory = "512"
#   service_config = templatefile("service.json", { cpu = local.cpu, memory = local.memory, port = "3000" })
# }

data "aws_iam_policy" "lambda_execution_role" {
   arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
} 
data "aws_iam_policy" "lambda_secrets_access" {
   arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
} 
data "aws_iam_policy" "apigw_cloudwatch_access" {
   arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
} 

# resource "aws_iam_role" "ecs_role"{
#   name = "hello_world_web_ecs_role"
#   assume_role_policy = file("ecs-assume-role.json")
# }

# resource "aws_iam_role_policy_attachment" "ecs_role_logging" {
#   role = aws_iam_role.ecs_role.id
#   policy_arn = data.aws_iam_policy.ecs_execution_role.id
# }

resource "aws_iam_role" "lambda_role" {
  name = "hello-world-back-end-lambda-role"
  assume_role_policy = file("lambda-assume-role.json")
}

resource "aws_iam_role_policy_attachment" "lambda_basic_execution_role" {
  role = aws_iam_role.lambda_role.id
  policy_arn = data.aws_iam_policy.lambda_execution_role.id
}

resource "aws_iam_role_policy_attachment" "lambda_secrets_mgr" {
  role = aws_iam_role.lambda_role.id
  policy_arn = data.aws_iam_policy.lambda_secrets_access.id
}

resource "aws_iam_role" "apigw_cloudwatch_role" {
  name = "apigw_cloudwatch"
  assume_role_policy = file("apigateway-assume-role.json")
}

resource "aws_iam_role_policy_attachment" "apigw_cloudwatch" {
  role = aws_iam_role.apigw_cloudwatch_role.id
  policy_arn = data.aws_iam_policy.apigw_cloudwatch_access.id
}

resource "aws_api_gateway_account" "demo" {
  cloudwatch_role_arn = aws_iam_role.apigw_cloudwatch_role.arn
}

resource "aws_cloudwatch_log_group" "apigw" {
  name              = "API-Gateway-Execution-Logs_${aws_apigatewayv2_api.mfa_service.id}/dev"
  retention_in_days = 30
}


# data "archive_file" "lambda_zip" {
#   type = "zip"
#   output_path = "/tmp/mfa_chck.zip"
#   source_dir = "mfa-check-lambda"
# }

# data "archive_file" "python_lambdas_zip" {
#   type = "zip"
#   output_path = "/tmp/python_lambdas.zip"
#   source_dir = "python-lambdas"
# }

resource "aws_lambda_function" "mfa_check_and_message_return" {
  filename = format("%s/python-lambdas/my-deployment-package.zip", path.module)
  function_name = "mfa_verify"
  role = aws_iam_role.lambda_role.arn
  handler = "apigw-lambdas.mfa_verify.mfa_verify_handler"
  source_code_hash = filesha256(format("%s/python-lambdas/my-deployment-package.zip", path.module))
  runtime = "python3.8"
  timeout = 900
  environment {
    variables = {
      HYPR_ACCESS_TOKEN_ARN=aws_secretsmanager_secret.hypr_key.arn
      HYPR_URL="gs-hypr-customer-dev.hypr.com"
      HYPR_APPLICATION="jJTest"
    }
  }
 }

resource "aws_lambda_permission" "mfa_check_and_message_return_v2"{
   statement_id = "AllowExecutionFromApiGatewayV2"
   action = "lambda:InvokeFunction"
   function_name = aws_lambda_function.mfa_check_and_message_return.function_name
   principal = "apigateway.amazonaws.com"
   source_arn = "${aws_apigatewayv2_api.mfa_service.execution_arn}/*/POST/mfa_check" 
 }

# data "archive_file" "mfa_push_zip" {
#    type = "zip"
#    output_path = "/tmp/mfa_psh.zip"
#    source_dir = "mfa-submitter-lambda"
#  }

resource "aws_lambda_function" "mfa_submitter" {
  filename = format("%s/python-lambdas/my-deployment-package.zip", path.module)
  function_name = "mfa_push"
  role = aws_iam_role.lambda_role.arn
  handler = "apigw-lambdas.mfa_push.mfa_push_handler"
  source_code_hash = filesha256(format("%s/python-lambdas/my-deployment-package.zip", path.module))
  runtime = "python3.8"
  timeout = 900
  environment {
    variables = {
      HYPR_ACCESS_TOKEN_ARN=aws_secretsmanager_secret.hypr_key.arn
      HYPR_URL="gs-hypr-customer-dev.hypr.com"
      HYPR_APPLICATION="jJTest"
    }
  }
}

resource "aws_lambda_permission" "mfa_submiktter_v2"{
   statement_id = "AllowExecutionFromApiGatewayV2"
   action = "lambda:InvokeFunction"
   function_name = aws_lambda_function.mfa_submitter.function_name
   principal = "apigateway.amazonaws.com"
   source_arn = "${aws_apigatewayv2_api.mfa_service.execution_arn}/*/POST/mfa_push" 
}

resource "aws_apigatewayv2_api" "mfa_service" {
  name          = "mfa-service"
  protocol_type = "HTTP"
  cors_configuration {
    # allow_credentials = "true"
    allow_headers = ["content-type", "authorization"]
    allow_origins = ["*"]
    allow_methods = ["*"]
  }
}

resource "aws_apigatewayv2_integration" "mfa_push" {
  api_id = aws_apigatewayv2_api.mfa_service.id
  integration_type = "AWS_PROXY"
  integration_method = "POST"
  integration_uri = aws_lambda_function.mfa_submitter.invoke_arn
  description = "MFA Push Integration"
}

resource "aws_apigatewayv2_route" "mfa_push"{
  api_id = aws_apigatewayv2_api.mfa_service.id
  route_key = "POST /mfa_push"
  target = "integrations/${aws_apigatewayv2_integration.mfa_push.id}"
  authorization_type = "JWT"
  authorizer_id = aws_apigatewayv2_authorizer.okta_jwt.id  
}

resource "aws_apigatewayv2_integration" "mfa_check" {
  api_id = aws_apigatewayv2_api.mfa_service.id
  integration_type = "AWS_PROXY"
  integration_method = "POST"
  integration_uri = aws_lambda_function.mfa_check_and_message_return.invoke_arn
  description = "MFA Check Integration"
}

resource "aws_apigatewayv2_route" "mfa_check"{
  api_id = aws_apigatewayv2_api.mfa_service.id
  route_key = "POST /mfa_check"
  target = "integrations/${aws_apigatewayv2_integration.mfa_check.id}"
  authorization_type = "JWT"
  authorizer_id = aws_apigatewayv2_authorizer.okta_jwt.id  
}

resource "aws_apigatewayv2_authorizer" "okta_jwt" {
  api_id = aws_apigatewayv2_api.mfa_service.id
  name = "okta-jwt_authorizer"
  authorizer_type = "JWT"
  identity_sources = ["$request.header.Authorization"]
  jwt_configuration {
    audience = ["api://default"]
    issuer = "https://dev-36113546.okta.com/oauth2/default"
  }
}



resource "aws_apigatewayv2_stage" "mfa_service_dev" {
  api_id = aws_apigatewayv2_api.mfa_service.id
  name = "dev"
  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.apigw.arn
    format = "$context.identity.sourceIp $context.identity.caller $context.identity.user [$context.requestTime] $context.httpMethod $context.resourcePath $context.protocol $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"
  }
}

resource "aws_apigatewayv2_deployment" "mfa_service_deployment"{
  depends_on = [aws_apigatewayv2_authorizer.okta_jwt, aws_apigatewayv2_integration.mfa_push, aws_apigatewayv2_integration.mfa_check, aws_apigatewayv2_route.mfa_push, aws_apigatewayv2_route.mfa_check, aws_apigatewayv2_stage.mfa_service_dev]
  api_id = aws_apigatewayv2_api.mfa_service.id
  description = "MFA Service Deployment"

  triggers = {
    redeployment = sha1(join(",",[
      jsonencode(aws_apigatewayv2_authorizer.okta_jwt), 
      jsonencode(aws_apigatewayv2_integration.mfa_push), 
      jsonencode(aws_apigatewayv2_integration.mfa_check), 
      jsonencode(aws_apigatewayv2_route.mfa_push), 
      jsonencode(aws_apigatewayv2_route.mfa_check)
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

# resource "aws_lambda_permission" "mfa_check_and_message_return"{
#    statement_id = "AllowExecutionFromApiGateway"
#    action = "lambda:InvokeFunction"
#    function_name = aws_lambda_function.mfa_check_and_message_return.function_name
#    principal = "apigateway.amazonaws.com"
#    source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.mfa_push_and_check.id}/*/${aws_api_gateway_method.mfa_check.http_method}/*" 
#  }


# resource "aws_lambda_permission" "mfa_submitter"{
#    statement_id = "AllowExecutionFromApiGateway"
#    action = "lambda:InvokeFunction"
#    function_name = aws_lambda_function.mfa_submitter.function_name
#    principal = "apigateway.amazonaws.com"
#    source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.mfa_push_and_check.id}/*/${aws_api_gateway_method.mfa_push.http_method}/*" 
#  }


# resource "aws_lambda_function" "cors_handler" {
#   filename = format("%s/python-lambdas/my-deployment-package.zip", path.module)
#   function_name = "cors_handler"
#   role = aws_iam_role.lambda_role.arn
#   handler = "apigw-lambdas.cors_handler.lambda_handler"
#   source_code_hash = filesha256(format("%s/python-lambdas/my-deployment-package.zip", path.module))
#   runtime = "python3.8"
#   timeout = 30
#  }

# resource "aws_lambda_permission" "mfa_submitter_and_check_cors"{
#    statement_id = "AllowExecutionFromApiGateway"
#    action = "lambda:InvokeFunction"
#    function_name = aws_lambda_function.cors_handler.function_name
#    principal = "apigateway.amazonaws.com"
#    source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.mfa_push_and_check.id}/*/${aws_api_gateway_method.mfa_push_cors.http_method}/*" 
# }

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

# resource "aws_cloudwatch_log_group" "apigw" {
#   name              = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.mfa_push_and_check.id}/dev"
#   retention_in_days = 30
# }


# resource "aws_api_gateway_rest_api" "mfa_push_and_check" {
#   name = "mfa_push_and_check"
# }

# resource "aws_api_gateway_resource" "mfa_check" {
#   parent_id   = aws_api_gateway_rest_api.mfa_push_and_check.root_resource_id
#   path_part   = "mfa_check"
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
# }

# resource "aws_api_gateway_method" "mfa_check" {
#   authorization = "NONE"
#   http_method   = "POST"
#   resource_id   = aws_api_gateway_resource.mfa_check.id
#   rest_api_id   = aws_api_gateway_rest_api.mfa_push_and_check.id
# }

# resource "aws_api_gateway_method" "mfa_check_cors" {
#   authorization = "NONE"
#   http_method   = "OPTIONS"
#   resource_id   = aws_api_gateway_resource.mfa_check.id
#   rest_api_id   = aws_api_gateway_rest_api.mfa_push_and_check.id
# }

# resource "aws_api_gateway_integration" "mfa_check" {
#   http_method = aws_api_gateway_method.mfa_check.http_method
#   resource_id = aws_api_gateway_resource.mfa_check.id
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
#   integration_http_method = "POST"
#   type                    = "AWS_PROXY"
#   uri                     = aws_lambda_function.mfa_check_and_message_return.invoke_arn
# }

# resource "aws_api_gateway_integration" "mfa_check_cors" {
#   http_method = aws_api_gateway_method.mfa_check_cors.http_method
#   resource_id = aws_api_gateway_resource.mfa_check.id
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
#   integration_http_method = "POST"
#   type                    = "AWS_PROXY"
#   uri                     = aws_lambda_function.cors_handler.invoke_arn
# }

# resource "aws_api_gateway_deployment" "mfa_push_and_check" {
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id

#   triggers = {
#     # NOTE: The configuration below will satisfy ordering considerations,
#     #       but not pick up all future REST API changes. More advanced patterns
#     #       are possible, such as using the filesha1() function against the
#     #       Terraform configuration file(s) or removing the .id references to
#     #       calculate a hash against whole resources. Be aware that using whole
#     #       resources will show a difference after the initial implementation.
#     #       It will stabilize to only change when resources change afterwards.
#     redeployment = sha1(jsonencode([
#       aws_api_gateway_resource.mfa_check.id,
#       aws_api_gateway_method.mfa_check.id,
#       aws_api_gateway_method.mfa_check_cors.id,
#       aws_api_gateway_integration.mfa_check.id,
#       aws_api_gateway_integration.mfa_check_cors.id,
#       aws_api_gateway_resource.mfa_push.id,
#       aws_api_gateway_method.mfa_push.id,
#       aws_api_gateway_method.mfa_push_cors.id,
#       aws_api_gateway_integration.mfa_push.id, 
#       aws_api_gateway_integration.mfa_push_cors.id
#       ]))
#   }

#   lifecycle {
#     create_before_destroy = true
#   }
# }

# resource "aws_api_gateway_stage" "mfa_push_and_check" {
#   depends_on = [aws_cloudwatch_log_group.apigw]
#   deployment_id = aws_api_gateway_deployment.mfa_push_and_check.id
#   rest_api_id   = aws_api_gateway_rest_api.mfa_push_and_check.id
#   stage_name    = "dev"
# }

# resource "aws_api_gateway_method_settings" "mfa_push_and_check_dev" {
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
#   stage_name = aws_api_gateway_stage.mfa_push_and_check.stage_name
#   method_path = "*/*"
#   settings {
#     metrics_enabled = true
#     logging_level = "INFO"
#   }
# }

# resource "aws_api_gateway_resource" "mfa_push" {
#   parent_id   = aws_api_gateway_rest_api.mfa_push_and_check.root_resource_id
#   path_part   = "mfa_push"
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
# }

# resource "aws_api_gateway_method" "mfa_push" {
#   authorization = "NONE"
#   http_method   = "POST"
#   resource_id   = aws_api_gateway_resource.mfa_push.id
#   rest_api_id   = aws_api_gateway_rest_api.mfa_push_and_check.id
# }

# resource "aws_api_gateway_method" "mfa_push_cors" {
#   authorization = "NONE"
#   http_method   = "OPTIONS"
#   resource_id   = aws_api_gateway_resource.mfa_push.id
#   rest_api_id   = aws_api_gateway_rest_api.mfa_push_and_check.id
# }

# resource "aws_api_gateway_integration" "mfa_push" {
#   http_method = aws_api_gateway_method.mfa_push.http_method
#   resource_id = aws_api_gateway_resource.mfa_push.id
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
#   integration_http_method = "POST"
#   type                    = "AWS_PROXY"
#   uri                     = aws_lambda_function.mfa_submitter.invoke_arn
# }

# resource "aws_api_gateway_integration" "mfa_push_cors" {
#   http_method = aws_api_gateway_method.mfa_push_cors.http_method
#   resource_id = aws_api_gateway_resource.mfa_push.id
#   rest_api_id = aws_api_gateway_rest_api.mfa_push_and_check.id
#   integration_http_method = "POST"
#   type                    = "AWS_PROXY"
#   uri                     = aws_lambda_function.cors_handler.invoke_arn
# }
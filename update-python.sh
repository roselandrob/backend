#!/bin/sh

cd python-lambdas/
rm my-deployment-package.zip 
cd packages/
zip -r ../my-deployment-package.zip .
cd ..
zip -g my-deployment-package.zip apigw-lambdas/*
cd ..

